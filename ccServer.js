var http = require('http');
var querystring = require('querystring');
var url         = require('url');
var mysql       = require('mysql');
var encrypToken = require('crypto');
var moment      = require('moment');
var util        = require('util');
var path        = require('path');
var mime        = require('mime');
var fs          = require('fs');
var formidable  = require('formidable');

var serverIP    = 'ec2-52-77-224-25.ap-southeast-1.compute.amazonaws.com';
var __dirname   = '/var/www/apk/files/';

var pool  = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : 'bbserverdbadmin',
    database : 'bbserverdbadmin'
});

function distance(p1, p2) {    
    var radlat1 = Math.PI * p1.shopLat/180;
    var radlat2 = Math.PI * p2.shopLat/180;
    var radlon1 = Math.PI * p1.shopLng/180;
    var radlon2 = Math.PI * p2.shopLng/180;
    var theta = p1.shopLng-p2.shopLng;
    var radtheta = Math.PI * theta/180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515 * 1.60934;
    
    var result = Math.round(dist*100)/100;
    //console.log(result);
    p2["distance"] = result;
    //console.log(p2);
    return result;
}

function distanceRad(p1, p2) {
    console.log(p1);
    console.log(p2);
    var radlat1   = Math.PI * p1.shopLat/180;
    var radlat2   = Math.PI * p2.shopLat/180;
    var radlon1   = Math.PI * p1.shopLng/180;
    var radlon2   = Math.PI * p2.shopLng/180;
    var radiusDis = p2.radius;
    var theta = p1.shopLng-p2.shopLng;
    var radtheta = Math.PI * theta/180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;

    // Meter and radiusDis in TB is also under Meter unit
    dist = dist * 1.609344 * 1000; // Meter

    var lastDist = dist-radiusDis;
    console.log('Orig: ' + dist + ' After: ' + lastDist);
    return (dist-radiusDis);
}

http.createServer(function (req, res) {
    // set up some routes 
    var urlParam = req.url.split('/').pop();
    if (urlParam.length == 0)
    {
        urlParam = '';
    }

    console.log(urlParam);
    var caseDeal = '/deals/' + urlParam;
    var caseFilter = '/filterItems/' + urlParam;
    var caseBankCard = '/bankCardTypes/' + urlParam;
    var caseCategories = '/categories/' + urlParam;
    var caseDealItem = '/deal/' + urlParam;
    
    switch(req.url) {
        case '/':
            console.log("[501] " + req.method + " to " + req.url);
            res.writeHead(501, "Not implemented", {'Content-Type': 'text/html'});
            res.end('<html><head><title>501 - Not implemented</title></head><body><h1>Not implemented!</h1></body></html>');
            break;

        case caseDeal:
            console.log("[501]: Users caseDeal requested: " + req.method );
            if (req.method == 'GET')
            {
                //Get user like items here                
                var url_parts = url.parse(req.url, true);
                var query     = url_parts.query;
                var lat       = query.lat;
                var lng       = query.lng;
                var offset    = query.offset;
                var limit     = query.limit;
                var filterBanks  = query.filterBanks;
                var filterBanksArr;
                var filterBankQuery='';
                var filterCats   = query.filterCats;
                var filterCatsArr;
                var filterCatsQuery='';
                var queryTemp = 'select dealItemId, bank_name as bankName, description, dealImgLink, ' +
                                'shop_table.shopName as shopName, dealStartDate, dealEndDate, shopLat, shopLng, ' +
                                'shopAddress, region, cat_table.categoryName as category, bank_name as bankCardType, distance, flag ' +
                                'from dealItem_table ' +
                                'left join bank_table ON bank_table.bankId = dealItem_table.bankCode ' +
                                'left join shop_table ON dealItem_table.shopCode = shop_table.shopId ' +
                                'left join cat_table ON dealItem_table.catCode = cat_table.categorySN ' +
                                'WHERE dealEndDate >= CURDATE() ';
                //LIMIT ' + limit +' OFFSET ' + offset;
                
                var fBank = 0;
                if (null == filterBanks)
                {
                    console.log('No filter bank');                    
                }
                else
                {                    
                    console.log(filterBanks);
                    var fBank = 1;
                    filterBanksArr = filterBanks.split(",");
                    for (i = 0; i < filterBanksArr.length; i++) {
                        var temp  = filterBanksArr[i].trim();
                        var temp1 = temp.toUpperCase();
                        if (i == 0) {                            
                            filterBankQuery += "bank_table.bank_name='" + temp1 + "'";
                        }
                        else {
                            filterBankQuery += " OR bank_table.bank_name='" + temp1 + "'";
                        }
                        
                    }
                                        
                    queryTemp += "AND ((" +  filterBankQuery +")) ";
                    console.log(queryTemp);
                }
                
                if (null == filterCats)
                {
                    console.log('No filter Cats');
                }
                else
                {
                    filterCatsArr = filterCats.split(",");
                    for (i = 0; i < filterCatsArr.length; i++) {
                        var temp2  = filterCatsArr[i].trim();
                        var temp3 = temp2.toUpperCase();
                        if (i == 0) {
                            filterCatsQuery += "cat_table.categoryName='" + temp3 + "'";
                        }
                        else {
                            filterCatsQuery += " OR cat_table.categoryName='" + temp3 + "'";
                        }                        
                    }
                    
                    /*
                    if (fBank == 0) {
                        queryTemp += "WHERE ((" +  filterCatsQuery +")) ";
                    }
                    else
                    {
                    */
                        queryTemp += "AND ((" +  filterCatsQuery +")) ";
                    //}
                                        
                    console.log(queryTemp);
                }
                
                console.log('Records: ' + lat + ' ' + lng + ' ' + offset + ' ' + limit + ' ' + filterBanks + ' ' + filterCats);

                pool.getConnection(function(err, connection) {                                               
                        
                        queryTemp +=" LIMIT " + limit + " OFFSET " + offset;
                        /*     
                        queryTemp +=" dealItem_table.bankCode = bank_table.bankId" +
                                    " AND dealItem_table.catCode = cat_table.categorySN" +
                                    " AND dealItem_table.shopCode = shop_table.shopId" +
                                    " LIMIT " + limit + " OFFSET " + offset;
                        */
                        console.log(queryTemp);
                        
                        connection.query( queryTemp, function(err, rows) {
                            // And done with the connection.                            
                            console.log('1111');
                            if (err)
                            {   
                                //throw err                                
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                var jData = {"message":'caseDeal', "code":'1'};
                                console.log(jData);
                                res.write(JSON.stringify(jData));
                                res.end();
                                connection.release();
                            }
                            else
                            {                                
                                if (rows.length > 0)
                                {
                                    var home = {"shopLat":lat, "shopLng":lng};
                                    //console.log(home)
                                    // Need to return the nearest 20 records
                                    rows.sort(function(a, b) {
                                        return distance(home,a)-distance(home,b);
                                    });

                                    console.log('Response caseDeal');
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify(rows));
                                    res.end();
                                    connection.release();
                                }
                                else
                                {
                                    // Kick out from Login page
                                    console.log('Call Err here 2...');
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    var jData = {"message":'caseDeal No data', "code":'4'};
                                    console.log(jData);
                                    res.write(JSON.stringify(jData));
                                    res.end();
                                    connection.release();
                                }
                            }
                        });
                    });

            }            
            else
            {
                res.writeHead(404, "Not found", {'Content-Type': 'text/html'});
                res.end('<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>');
                console.log("[404] " + req.method + " to " + req.url);
            }
            break;

        case caseDealItem:
            console.log("[501]: caseDealItem requested: " + req.method );
            if (req.method == 'GET')
            {                
                var url_parts  = url.parse(req.url, true);
                var query      = url_parts.query;
                var dealItemId = query.dealItemId;
                
                console.log('Records: ' + dealItemId);
                
                pool.getConnection(function(err, connection) {
                        // Use the connection
                        //console.log(connection);                        
                        var queryTemp = "select dealItemId, dealTC, dealURL, shopContact as contactNumber from dealItem_table, shop_table where dealItemId=" + dealItemId
                                        + " and shop_table.shopId = dealItem_table.shopCode";
                                        
                        console.log(queryTemp);
                        connection.query( queryTemp, function(err, rows) {
                            // And done with the connection.                                                        
                            if (err)
                            {   
                                //throw err                                
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                var jData = {"message":'caseDealItem', "code":'1'};
                                console.log(jData);
                                res.write(JSON.stringify(jData));
                                res.end();
                                connection.release();
                            }
                            else
                            {
                                console.log("No Err... " + rows.length);
                                if (rows.length > 0)
                                {                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    console.log('Response caseDealItem');
                                    res.write(JSON.stringify(rows));
                                    res.end();
                                    connection.release();
                                }
                                else
                                {
                                    // Kick out from Login page                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    var jData = {"message":'caseDealItem No records', "code":'4'};
                                    console.log(jData);
                                    res.write(JSON.stringify(jData));
                                    res.end();
                                    connection.release();
                                }
                            }
                        });
                    });

            }            
            else
            {
                res.writeHead(404, "Not found", {'Content-Type': 'text/html'});
                res.end('<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>');
                console.log("[404] " + req.method + " to " + req.url);
            }
            break;
        
        case caseCategories:
            console.log("[501]: caseCategories requested: " + req.method );
            if (req.method == 'GET')
            {                
                pool.getConnection(function(err, connection) {
                        // Use the connection
                        //console.log(connection);
                        var queryTemp = "select * from cat_table";                                                                                
                        console.log(queryTemp);
                        connection.query( queryTemp, function(err, rows) {
                            // And done with the connection.                                                        
                            if (err)
                            {   
                                //throw err                                
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                var jData = {"message":'caseCategories', "code":'1'};
                                console.log(jData);
                                res.write(JSON.stringify(jData));
                                res.end();
                                connection.release();
                            }
                            else
                            {
                                console.log("No Err... " + rows.length);
                                if (rows.length > 0)
                                {                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    console.log('Response caseCategories');
                                    res.write(JSON.stringify(rows));
                                    res.end();
                                    connection.release();
                                }
                                else
                                {
                                    // Kick out from Login page                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    var jData = {"message":'caseCategories No records', "code":'4'};
                                    console.log(jData);
                                    res.write(JSON.stringify(jData));
                                    res.end();
                                    connection.release();
                                }
                            }
                        });
                    });

            }            
            else
            {
                res.writeHead(404, "Not found", {'Content-Type': 'text/html'});
                res.end('<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>');
                console.log("[404] " + req.method + " to " + req.url);
            }
            break;
        
        case caseBankCard:
            console.log("[501]: caseBankCard requested: " + req.method );
            if (req.method == 'GET')
            {                
                pool.getConnection(function(err, connection) {
                        // Use the connection
                        //console.log(connection);
                        var queryTemp = "select cardId as bankCardSN, bank_table.bank_name as bankName, cardName from cards_table, bank_table "
                                        + "where cards_table.bankId = bank_table.bankId"; 
                                        
                        console.log(queryTemp);
                        connection.query( queryTemp, function(err, rows) {
                            // And done with the connection.                                                        
                            if (err)
                            {   
                                //throw err                                
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                var jData = {"message":'caseBankCard', "code":'1'};
                                console.log(jData);
                                res.write(JSON.stringify(jData));
                                res.end();
                                connection.release();
                            }
                            else
                            {
                                console.log("No Err... " + rows.length);
                                if (rows.length > 0)
                                {                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    console.log('Response caseBankCard');
                                    res.write(JSON.stringify(rows));
                                    res.end();
                                    connection.release();
                                }
                                else
                                {
                                    // Kick out from Login page                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    var jData = {"message":'caseBankCard No records', "code":'4'};
                                    console.log(jData);
                                    res.write(JSON.stringify(jData));
                                    res.end();
                                    connection.release();
                                }
                            }
                        });
                    });

            }            
            else
            {
                res.writeHead(404, "Not found", {'Content-Type': 'text/html'});
                res.end('<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>');
                console.log("[404] " + req.method + " to " + req.url);
            }
            break;
        
        case caseFilter:
            console.log("[501]: caseFilter requested: " + req.method );
            if (req.method == 'GET')
            {                
                pool.getConnection(function(err, connection) {
                        // Use the connection
                        //console.log(connection);
                        var queryTemp = "select @rownum := @rownum + 1 as itemSN, 'bankCard' as itemType, bank_name as item " 
                                        + "from bank_table " 
                                        + "union all "
                                        + "select @rownum := @rownum + 1 as itemSN, 'cat' as itemType, categoryName as item "
                                        + "from cat_table "
                                        + "cross join (select @rownum :=0) r";
                                        
                        console.log(queryTemp);
                        connection.query( queryTemp, function(err, rows) {
                            // And done with the connection.                                                        
                            if (err)
                            {   
                                //throw err                                
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                var jData = {"message":'casePointsURL GET in TBL_USER', "code":'1'};
                                console.log(jData);
                                res.write(JSON.stringify(jData));
                                res.end();
                                connection.release();
                            }
                            else
                            {
                                console.log("No Err... " + rows.length);
                                if (rows.length > 0)
                                {                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    console.log('Response caseFilter');
                                    res.write(JSON.stringify(rows));
                                    res.end();
                                    connection.release();
                                }
                                else
                                {
                                    // Kick out from Login page                                    
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    var jData = {"message":'caseFilter No records', "code":'4'};
                                    console.log(jData);
                                    res.write(JSON.stringify(jData));
                                    res.end();
                                    connection.release();
                                }
                            }
                        });
                    });

            }            
            else
            {
                res.writeHead(404, "Not found", {'Content-Type': 'text/html'});
                res.end('<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>');
                console.log("[404] " + req.method + " to " + req.url);
            }
            break;
        
        default:
            res.writeHead(404, "Not found", {'Content-Type': 'text/html'});
            res.end('<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>');
            console.log("[404] " + req.method + " to " + req.url);
    };

}).listen(1668);
console.log('Server running at http://ec2-52-77-224-25.ap-southeast-1.compute.amazonaws.com:1668/');